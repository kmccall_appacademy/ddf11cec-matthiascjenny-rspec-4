class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    if (self.seconds % 60) > 9
      result = (self.seconds % 60).to_s
    else
      result = "0#{(self.seconds % 60).to_s}"
    end
    if ((self.seconds / 60) % 60) > 9
      result = "#{((self.seconds / 60) % 60).to_s}:#{result}"
    else
      result = "0#{((self.seconds / 60) % 60).to_s}:#{result}"
    end
    if ((self.seconds / 60) % 60) > 9
      result = "#{(self.seconds / 3600).to_s}:#{result}"
    else
      result = "0#{(self.seconds / 3600).to_s}:#{result}"
    end
    return result
  end

end
