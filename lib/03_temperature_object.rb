class Temperature
  attr_accessor :f, :c

  def initialize(hash = {f: nil, c: nil})
    @f = hash[:f]
    @c = hash[:c]
  end

  def in_fahrenheit
    if @f == nil
      @f = (@c*9/5.0) + 32
    end
    @f
  end

  def in_celsius
    if @c == nil
      @c = (@f - 32)*5/9.0
    end
    @c
  end

  def self.from_celsius(n)
    Temperature.new({c: n})
  end

  def self.from_fahrenheit(n)
    Temperature.new({f: n})
  end

end

class Celsius < Temperature
  attr_accessor :c

  def initialize(c)
    @c = c
  end
end

class Fahrenheit < Temperature
  attr_accessor :f

  def initialize(f)
    @f = f
  end
end
