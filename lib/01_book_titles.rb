class Book
  attr_accessor :title

  def initialize(title = "")
    @title = title
  end

  def title
    @title.capitalize!
    arr = @title.split(" ")
    not_cap = ["a", "an", "the", "and", "but", "or", "for", "nor", "on",\
      "at", "to", "from", "by", "in", "of"]
      arr.each_with_index do |el,i|
        unless not_cap.include? el
          arr[i] = el.capitalize
        end
      end
      return arr.join(" ")
  end

end
