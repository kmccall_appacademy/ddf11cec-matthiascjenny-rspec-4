class Dictionary
  attr_accessor :dict

  def initialize(dict={})
    @dict = dict
  end

  def entries
    return @dict
  end

  def add(entr)
    if entr.is_a? String
      @dict[entr] = nil
    else
      entr.each { |k,v| @dict[k] = v }
    end
  end

  def keywords
    return @dict.keys.sort
  end

  def include?(str)
    self.keywords.include?(str)
  end

  def find(str)
    hsh = Hash.new
    matches = []
    self.keywords.each do |el|
      if substring?(str,el)
        matches.push(el)
      end
    end
    matches.each { |el| hsh[el] = @dict[el]}
    return hsh
  end

  def substring?(s1,s2)
    s1.each_char.with_index do |ch,i|
      if s2[i] != ch
        return false
      end
    end
    return true
  end

  def printable
    kwrds = self.keywords
    result = "[#{kwrds[0]}] \"#{@dict[kwrds[0]]}\""
    kwrds.shift
    kwrds.each do |k|
       result << "\n[#{k}] \"#{@dict[k]}\""
    end
    result
  end

end
