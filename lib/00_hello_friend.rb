class Friend
  def greeting(whom = nil)
    if whom == nil
      return "Hello!"
    else
      return "Hello, #{whom}!"
    end
  end

end
